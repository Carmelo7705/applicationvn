const express = require('express');
const app = express();

const mongoose = require('mongoose');

const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');

const api = require('./routes/api');

//Settings
require('dotenv').config();

app.use(bodyParser.urlencoded({
    extended: false
}));

//DB
mongoose.connect(process.env.mongoURI, {useNewUrlParser: true})
    .then( () => {
        console.log(`Database connected successfully ${process.env.mongoURI}`);
    })
    .catch(err => console.log(`Unable to connect with de database ${err}`));

//Middlewares routes & translates API'S
app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.json());

//Passport middleware
app.use(passport.initialize());

//Bring in the passport strategy
require('./config/passport')(passport);

//api routes
app.use('/', api); //Prefix Route

app.listen(process.env.PORT, () => {
    console.log('server ready in port: ' + process.env.PORT);
});