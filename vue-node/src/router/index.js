import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/login.vue'
import Register from '../views/register.vue';
import Profile from '../views/profile.vue';

import store from '../store';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login,
    meta: {
      requiresGuest: true
    }
  },
  {
    path: '/register',
    name: 'register',
    component: Register,
    meta: {
      requiresGuest: true
    }
  },
  {
    path: '/profile',
    name: 'profile',
    component: Profile,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/home',
    name: 'home',
    component: Home,
    meta: {
      requiresAuth: true
    }
  }
  /*{
    path: '/about',
    name: 'about',
    component: () => import('../views/About.vue')
  }*/
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach( (to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) { //Aquellos que se necesite estar logueado 
    if(!store.getters.isLoggedIn) { //y efectivamente, isLogged sea false.
      //Redirect to the loginpage
      next('/');
    } else {
      next();
    }
  } else if(to.matched.some(record => record.meta.requiresGuest)) { //Aquellos que no necesiten estar logueados.
    if(store.getters.isLoggedIn) { //y efectivamente, isLogged sea true.
      //Redirect to the profile page.
      next('/profile');
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router
