import Vue from 'vue'
import Vuex from 'vuex'

import config from '../config/config';
import axios from 'axios';
import router from '../router';


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: localStorage.getItem('token') || '',
    user: {},
    status: '',
    error: null
  },
  getters: {
    /*isLoggedIn : (state) => {
      if(state.token != '') {
        return true;
      } else {
        return false;
      }
    }*/
    isLoggedIn : state => !!state.token,
    authState: state => state.status,
    user: state => state.user,
    error: state => state.error
  },

  mutations: {

    auth_request(state) {
      state.status = 'Loading...';
      state.error = null;
    },

    auth_success(state, token, user) {
      state.token = token;
      state.user = user;
      state.error = null;
      state.status = 'success'
    },

    auth_error(state, err) {
      state.error = err.response.data.msg;
    },

    register_request(state) {
      state.error = null;
      state.status = 'Loading...';
    },

    register_success(state) {
      state.status = 'success'
      state.error = null;
    },

    register_error(state, err) {
      state.error = err.response.data.msg;
    },

    profile_request(state) {
      state.status = 'Loading...';
    },

    profile_success(state, user) {
      state.status = 'success';
      state.user = user;
    },

    logout(state) {
      state.error = null;
      state.status = '';
      state.token = '';
      state.user = '';
    }

  },
  actions: {

    async login({ commit }, user) {
      
      commit('auth_request');
      try {
        let res = await axios.post(config.APIURL + 'signin', user);

        if(res.data.success) {
          const token = res.data.token;
          const user = res.data.user;
          //store token in localstorage
          localStorage.setItem('token', token);
          //Set the axios defaults
          axios.defaults.headers.common['Authorization'] = token;
          commit('auth_success', token, user);
        }

        return res;

      } catch(err) {
        commit('auth_error', err);
      }
    },

    async register({ commit }, data) {

      commit('register_request');

      try {
        let res = await axios.post(config.APIURL + 'register', data);

        if(res.data.success) {
          commit('register_success');
        }

        return res;
      } catch(err) {
        commit('register_error', err);
      }
    },

    async getProfile({ commit }) {
      commit('profile_request');

      let res = await axios.get(`${config.APIURL}profile`);

      const user = res.data.user;
      commit('profile_success', user);
    },

    async logout({ commit }) {

      await localStorage.removeItem('token');
      localStorage.removeItem('user_id');
      commit('logout');
      delete axios.defaults.headers.common['Authorization'];
      router.push('/');
      return;
    }
  }
})
