const User = require('../models/User');
const Key = require('../config/config').secret;

const bcrypt  = require('bcryptjs');
const jwt = require('jsonwebtoken');

let _this = module.exports = {
    
    register: (req, res) => {
        let {
            name,
            username,
            email,
            password,
            confirm_password
        } = req.body;


        if(password !== confirm_password) {
            return res.status(400).json({
                msg: "Password do not match."
            });
        }

        //Check unique username
        User.findOne({
            username: username
        }).then( (user) => {
            if(user) {
                return res.status(400).json({
                    msg: "Username is already taken."
                });
            }
        });

        //check unique email
        User.findOne({
            email: email
        }).then( (email) => {
            if(email) {
                return res.status(400).json({
                    msg: "Email is already registered. Did you forgot your password."
                });
            }
        });

        //The data is valid and we can the new user

        let data = new User({
            name,
            username,
            email,
            password
        });

        //Hash the password 
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(data.password, salt, (err, hash) => {
                if(err) throw err;

                data.password = hash;
                data.save();
                res.status(201).json({
                    success: true,
                    msg: "User registered successfully."
                });
            });
        });
    },

    login: (req, res) => {
        let {
            username,
            password
        } = req.body;

        User.findOne({
            username: username
        }).then( (user) => {
            if(!user) {
                return res.status(404).json({
                    msg: "Username is not found.",
                    success: false
                });
            }

            //compare password if user it's true
            bcrypt.compare(password, user.password).then(isMatch => {
                if(isMatch) {
                    //User's password is correct. Need to send the JSON token fot that user
                    const payload = {
                        _id: user._id,
                        username: user.username,
                        name: user.name,
                        email: user.email
                    };

                    jwt.sign(payload, Key, {
                        expiresIn: 604800,
                    },  (err, token) => {
                        res.status(200).json({
                            success: true,
                            token: `Bearer ${token}`,
                            user: user,
                            msg: 'Welcome User'
                        });
                    });

                } else {
                    return res.status(404).json({
                        msg: "Incorrect Password.",
                        success: false
                    });
                }
            });
        });
    },

    profile: (req, res) => {
        let user = req.user;
        return res.json({
            user: user
        });
    }
}