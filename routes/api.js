const express = require('express');
const router = express.Router();

const passport = require('passport');

const vueCtrl = require('../controllers');

//Routes Application

/*router.get();
router.post();
router.put();
router.delete();*/

router.post('/register', vueCtrl.UserController.register);
router.post('/signin', vueCtrl.UserController.login);

//Routes Authenticated
router.get('/profile/', passport.authenticate('jwt',{ session: false }),
     vueCtrl.UserController.profile); //Usamos de passport proteger las rutas.



module.exports = router;